# anonscm rewrite map generator

This repository is the source of the anonscm.debian.org to salsa.debian.org
rewrite map. Your redirects will be live at most within a week after the
merge (see
[announce](https://lists.debian.org/debian-devel-announce/2018/06/msg00001.html).
The redirect is staying even after alioth being gone now.

The existance of this list should not mean that VCS control fields cannot
get updated with the next upload. Please do not ask for removals of the maps
before enough time has passed, so that most people with checkouts can be
properly notified via git pull's warnings that they need to update their
URLs. Consider that removing the maps will render any such URLs in the wild
into dead URLs, so if in doubt, do not remove them.

## Definition files

A definition file is a simple file that is organized in two columns. First
column contains the original repo beginning with the alioth project (without
the trailing ".git") and the second column contains the new salsa group and
the project name. You can also add comments with lines starting with #.

Example:
```
   # this is a comment 
   collab-maint/dealer debian/dealer
``` 

Definitions are organized in files with filenames ending on .conf in the definitions
folder. 

## Contribute

If you want to get your migrated project redirected to salsa send a pull-request
(in gitlab parlance, a "merge request") against the
[AliothRewriter Project](https://salsa.debian.org/salsa/AliothRewriter). 
If you have only one or two rewrites append them to the *general.conf* file. If you have
more redirects just create a new file ending with *.conf*. 

### Things to keep in mind

Redirection only works for https/https urls and doesn't
affect git:// or git+ssh://. It is recommended to lock repos
on alioth after migration. See `/git/pkg-postgresql/postgresql-common.git/hooks/pre-receive`
on alioth for an example of such a lock.
